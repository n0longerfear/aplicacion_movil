package com.example.pizeria_dimarco;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class ClaseRegistro extends AppCompatActivity{
    private EditText editTextTextPersonName;
    private EditText editTextTextPersonName2;
    private EditText editTextDate;
    private EditText editTextTextPersonDni;
    private EditText editTextNumber;
    private EditText editTextTextPersoncorreo;
    private EditText editTextTextPassword;
    private Button buttonContinuar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registro);

        editTextTextPersonName = findViewById(R.id.editTextTextPersonName);
        editTextTextPersonName2 = findViewById(R.id.editTextTextPersonName2);
        editTextDate = findViewById(R.id.editTextDate);
        editTextTextPersonDni = findViewById(R.id.editTextTextPersonDni);
        editTextNumber = findViewById(R.id.editTextNumber);
        editTextTextPersoncorreo = findViewById(R.id.editTextTextPersonCorreo);
        editTextTextPassword = findViewById(R.id.editTextTextPassword);
        buttonContinuar = findViewById(R.id.buttonContinuar);

        buttonContinuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (v.getContext(), ClaseCentral.class);
            }
        });
    }
}