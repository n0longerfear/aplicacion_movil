package com.example.pizeria_dimarco;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
public class ClaseLogin extends AppCompatActivity{

    private EditText correoEditText;
    private EditText passwordEditText;
    private Button continuarButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        correoEditText = findViewById(R.id.editTextTextPersonCorreo);
        passwordEditText = findViewById(R.id.editTextTextPassword);
        continuarButton = findViewById(R.id.buttonContinuar);

        continuarButton.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                Intent intent = new Intent (v.getContext(), ClaseCentral.class);
            }
        });
    }
}



