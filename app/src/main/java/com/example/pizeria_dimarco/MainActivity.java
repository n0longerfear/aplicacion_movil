package com.example.pizeria_dimarco;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button Registro = findViewById(R.id.registro);
        Registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setContentView(R.layout.registro);
            }
        });

        Button Login = findViewById(R.id.button2);
        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setContentView(R.layout.login);
            }
        });
}

    /**
     * @Override
     * protected void onCreate(Bundle savedInstanceState) {
     *     super.onCreate(savedInstanceState);
     *     setContentView(R.layout.activity_main);
     *
     *     Button btn = findViewById(R.id.Button);
     *     btn.setOnClickListener(new View.OnClickListener() {
     *         @Override
     *         public void onClick(View v) {
     *             Intent intent = new Intent(MainActivity.this, Activity2.class);
     *             startActivity(intent);
     *         }
     *     });
     * }
     */
}